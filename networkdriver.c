/*
	Parsa Bagheri
	ID: 951406684
	CIS 415, Project 2

	This file is all my work, the header files that are included, however, were provided by professor Joe Sventek
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "networkdriver.h"
#include "packetdescriptor.h"
#include "destination.h"
#include "pid.h"
#include "freepacketdescriptorstore.h"
#include "freepacketdescriptorstore__full.h"
#include "networkdevice.h"
#include "BoundedBuffer.h"
#include "diagnostics.h"

/*size of the buffers*/

/* 
	#PDs = 36
	total #receive_b = 2 *(MAX_PID + 1) = 22
	send_b = 10
	pool_size = 4

	22 + 10 + 4 <= 36
*/
#define RECEIVE_BUFFER_SIZE 2
#define POOL_SIZE 4
#define SEND_BUFFER_SIZE 10


/*maximum number of tries if failed to send*/
#define MAX_TRIES 3

/*thread*/
pthread_t send_thread;
pthread_t receive_thread;

/*buffers*/
BoundedBuffer *send_b;
BoundedBuffer *pool = NULL;
BoundedBuffer *receive_b[MAX_PID + 1];

/*free packet descriptor store copy for local use*/
FreePacketDescriptorStore *_fpds;

/*network device copy, will be set upon initializing*/
NetworkDevice *_nd;

/*routines for threads*/
void *send()
{
	PacketDescriptor *pd;
	int result;
	int i;

	/*forever loop*/
	while(1){

		send_b->blockingRead(send_b, (void **)&pd);/*send buffer might be empty => blocking*/
		/*get network device to send packet to network*/
		/*try resending for the maximum number of tries if failed to send*/
		/*if still failed to send, give up*/
		for(i=0; i<MAX_TRIES; i++){
			result = _nd->sendPacket(_nd, pd);
			if(result)
				DIAGNOSTICS("[DRIVER> Info: Sent a packet after %d tries\n", (i+1));
				break;
		}
		if(!result)
			DIAGNOSTICS("[DRIVER> Info: Failed to send packet\n");
		/*try a nonblocking put in the pool/cache, if unsuccessful (cache is full), put it in the store*/
		if(pool == NULL || !pool->nonblockingWrite(pool, pd)){
			_fpds->nonblockingPut(_fpds, pd); /*fpds is an unbounded container so a nonblocking put always succeeds*/
		}
	}
}
void *receive()
{
	
	PacketDescriptor *pd;
	PID pid;
	while(1){
	/*try a nonblocking read from the pool/cache, if unsuccessful (cache is empty), get it from store*/
		if(pool == NULL || !pool->nonblockingRead(pool, (void **)&pd)){
			_fpds->blockingGet(_fpds, &pd);
		}
		initPD(pd); /*initialize it to be empty*/
		_nd->registerPD(_nd, pd); /*register it with the network device*/
		_nd->awaitIncomingPacket(_nd);/*block the thread and wait for an incoming packet data and fill pd with it*/
		pid = getPID(pd);
		DIAGNOSTICS("[DRIVER> Packet received for application %d\n", pid);
		if(!receive_b[pid]->nonblockingWrite(receive_b[pid], (void *)pd)){/*can't block, if it's full discard the packet*/
			if(pool == NULL || !pool->nonblockingWrite(pool, (void *)pd))
				_fpds->nonblockingPut(_fpds, pd); /*fpds is an unbounded container so a nonblocking put always succeeds*/
		}
	}

}
/*end routines*/

void init_network_driver(NetworkDevice               *nd, 
                         void                        *mem_start, 
                         unsigned long               mem_length,
                         FreePacketDescriptorStore **fpds_ptr)
{
	int i;
	/*initializing the fpds*/
	if((_fpds = FreePacketDescriptorStore_create(mem_start, mem_length)) == NULL){
		DIAGNOSTICS("Error occured in creating free packet descriptor store.\n\tInitialization of networkdriver failed.\n");
		exit(1); /*unsuccessful termination*/
	}

	/*initializing threads*/
	if(pthread_create(&send_thread, NULL, &send, NULL) != 0){
		DIAGNOSTICS("Error occured in creating send thread.\n\tInitialization of networkdriver failed.\n");
		exit(1); /*unsuccessful termination*/
	}
	if(pthread_create(&receive_thread, NULL, &receive, NULL) != 0){
		DIAGNOSTICS("Error occured in creating receive thread.\n\tInitialization of networkdriver failed.\n");
		exit(1); /*unsuccessful termination*/
	}
	/*end thread_init*/

	/*initializing buffers*/
	if((send_b = BoundedBuffer_create(SEND_BUFFER_SIZE)) == NULL){
		DIAGNOSTICS("Error occured in creating send buffer.\n\tInitialization of networkdriver failed.\n");
		exit(1); /*unsuccessful termination*/
	}

	for(i=0; i<(MAX_PID+1); i++){
		if((receive_b[i] = BoundedBuffer_create(RECEIVE_BUFFER_SIZE)) == NULL){
			DIAGNOSTICS("Error occured in creating receive buffer.\n\tInitialization of networkdriver failed.\n");
			exit(1); /*unsuccessful termination*/
		}
	}

	if((pool = BoundedBuffer_create(POOL_SIZE)) == NULL){
		/* unlikely, but in case it happens do it without cache/pool*/
		DIAGNOSTICS("Failed to create pool. Initialize without pool.\n");
	}else{
		/*fill pool with packet descriptors from store*/
		PacketDescriptor *pd;
		for(i=0; i<POOL_SIZE; i++){
			_fpds->blockingGet(_fpds, &pd);
			pool->blockingWrite(pool, pd);
		}
	}

	/*end buffer_init*/
	_nd = nd;
	*fpds_ptr = _fpds;
}

void blocking_get_packet(PacketDescriptor **pd, PID pid)
{
	BoundedBuffer *rb = receive_b[pid];
	rb->blockingRead(rb, (void **)pd);
}
int  nonblocking_get_packet(PacketDescriptor **pd, PID pid)
{
	BoundedBuffer *rb = receive_b[pid];
	return rb->nonblockingRead(rb, (void **)pd);
}
void blocking_send_packet(PacketDescriptor *pd)
{
	send_b->blockingWrite(send_b, (void *)pd);
}
int  nonblocking_send_packet(PacketDescriptor *pd)
{
	return send_b->nonblockingWrite(send_b, (void *)pd);
}